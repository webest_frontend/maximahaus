var $body,
    windowHeight,
    windowWidth,
    mediaPoint1 = 1024,
    mediaPoint2 = 768,
    mediaPoint3 = 480,
    mediaPoint4 = 320;

$(document).ready(function ($) {
    $body = $('body');
    windowWidth = $(window).width();
    windowHeight = $(window).height();

    //developer funcitons
    //pageWidget(['index']);
    //getAllClasses('html','.elements_list');
});

$(window).on('load', function () {
    loadFunc();
});

$(window).on('resize', function () {
    resizeFunc();
});

$(window).on('scroll', function () {
    scrollFunc();
});

function loadFunc() {

}

function resizeFunc() {
    updateSizes();
}

function scrollFunc() {
    updateSizes();
}

function updateSizes() {
    windowWidth = $(window).width();
    windowHeight = $(window).height();
}

if ('objectFit' in document.documentElement.style === false) {
    document.addEventListener('DOMContentLoaded', function () {
        Array.prototype.forEach.call(document.querySelectorAll('img[data-object-fit]'), function (image) {
            (image.runtimeStyle || image.style).background = 'url("' + image.src + '") no-repeat 50%/' + (image.currentStyle ? image.currentStyle['object-fit'] : image.getAttribute('data-object-fit'));

            image.src = 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'' + image.width + '\' height=\'' + image.height + '\'%3E%3C/svg%3E';
        });
    });
}

//Functions for development
function getAllClasses(context, output) {
    var finalArray = [],
        mainArray = [],
        allElements = $(context).find($('*'));//get all elements of our page
    //If element has class push this class to mainArray
    for (var i = 0; i < allElements.length; i++) {
        var someElement = allElements[i],
            elementClass = someElement.className;
        if (elementClass.length > 0) {//if element have not empty class
            //If element have multiple classes - separate them
            var elementClassArray = elementClass.split(' '),
                classesAmount = elementClassArray.length;
            if (classesAmount === 1) {
                mainArray.push('.' + elementClassArray[0] + ' {');
            } else {
                var cascad = '.' + elementClassArray[0] + ' {';
                for (var j = 1; j < elementClassArray.length; j++) {
                    cascad += ' &.' + elementClassArray[j] + ' { }';
                }
                mainArray.push(cascad);
            }
        }
    }

    //creating finalArray, that don't have repeating elements
    var noRepeatingArray = unique(mainArray);
    noRepeatingArray.forEach(function (item) {
        var has = false;
        var preWords = item.split('&');
        for (var i = 0; i < finalArray.length; ++i) {
            var newWords = finalArray[i].split('&');
            if (newWords[0] == preWords[0]) {
                has = true;
                for (var j = 0; j < preWords.length; ++j) {
                    if (newWords.indexOf(preWords[j]) < 0) {
                        newWords.push(preWords[j]);
                    }
                }
                finalArray[i] = newWords.join('&');
            }
        }
        if (!has) {
            finalArray.push(item);
        }
    });
    for (var i = 0; i < finalArray.length; i++) {
        $('<div>' + finalArray[i] + ' }</div>').appendTo(output);
    }


    //function that delete repeating elements from arrays, for more information visit http://mathhelpplanet.com/static.php?p=javascript-algoritmy-obrabotki-massivov
    function unique(A) {
        var n = A.length, k = 0, B = [];
        for (var i = 0; i < n; i++) {
            var j = 0;
            while (j < k && B[j] !== A[i]) j++;
            if (j == k) B[k++] = A[i];
        }
        return B;
    }
}

function pageWidget(pages) {
    var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
    widgetWrap.prependTo("body");
    for (var i = 0; i < pages.length; i++) {
        if (i == 0) {
            $('<li class="widget_item"><a class="widget_link" href="/' + pages[i] + '.html' + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
        } else {
            $('<li class="widget_item"><a class="widget_link" href="/' + pages[i] + '/' + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
        }

    }
    var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:absolute;top:0;left:0;z-index:9999;padding:10px 20px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_item{padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
    widgetStilization.prependTo(".widget_wrap");
}

// Пример подключения
//= modules/test-modules.js

// Подключение SVG на продакшен перенести его код в footer выше подключения всех скриптов
;( function (window, document) {
    'use strict';

    var file = '/img/sprite/sprite.svg',
        revision = 2;

    if (!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect)
        return true;

    var isLocalStorage = 'localStorage' in window && window['localStorage'] !== null,
        request,
        data,
        SVG_container = document.getElementById('SVG_container'),
        insertIT = function () {
            SVG_container.insertAdjacentHTML('afterbegin', data);
        },
        insert = function () {
            if (document.body) insertIT();
            else document.addEventListener('DOMContentLoaded', insertIT);
        };

    if (isLocalStorage && localStorage.getItem('inlineSVGrev') == revision) {
        data = localStorage.getItem('inlineSVGdata');
        if (data) {
            insert();
            return true;
        }
    }

    try {
        request = new XMLHttpRequest();
        request.open('GET', file, true);
        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                data = request.responseText;
                insert();
                if (isLocalStorage) {
                    localStorage.setItem('inlineSVGdata', data);
                    localStorage.setItem('inlineSVGrev', revision);
                }
            }
        }
        request.send();
    }
    catch (e) {
    }

}(window, document) );

//phone mask
$('.phone-mask').mask('+7 (999) 999-99-99');

//slider allcity update
city_slider = {
    init : function () {
        this.event_handler();
    },

    event_handler: function () {
        var self = this;

        $('.js-swiper-city').each(function () {
            var name_slider = $(this).data('slider');
            window[name_slider] = new Swiper($(this), {
                slidesPerView: 5,
                spaceBetween: 70,
                // watchOverflow: true,
                // disabledClass: 'swiper-button-disabled',
                // navigation: {
                //     nextEl: '.js-swiper-city-next',
                //     prevEl: '.js-swiper-city-prev'
                // },
                breakpoints: {
                    1369: {
                        slidesPerView: 4,
                        spaceBetween: 50
                    },
                    767: {
                        slidesPerView: 3,
                        spaceBetween: 50
                    },
                    575: {
                        slidesPerView: 2,
                        spaceBetween: 20
                    }
                }
            });
        });

        $(document).on('click', '.js-swiper-city-prev', function () {
            self.prev_slider($(this).data('this-slider'))
        });

        $(document).on('click', '.js-swiper-city-next', function () {
            self.next_slider($(this).data('this-slider'))
        });

        $(document).on('click', '.js-btn-allcity', function (event) {
            self.toogle_hidden($(this));
        });
    },

    next_slider: function (slider) {
        window[slider].slideNext();
    },

    prev_slider: function (slider) {
        window[slider].slidePrev();
    },

    toogle_hidden : function (btn) {
        if(btn.hasClass('collapsed')){
            btn.find('span').text('Все города')
        } else {
            btn.find('span').text('Свернуть')
        }
        this.slider_update();
    },

    slider_update : function () {
        $('.js-swiper-city--hidden').each(function () {
            var name_slider = $(this).data('slider');
            window[name_slider] = new Swiper($(this), {
                slidesPerView: 5,
                spaceBetween: 70,
                breakpoints: {
                    1369: {
                        slidesPerView: 4,
                        spaceBetween: 50
                    },
                    767: {
                        slidesPerView: 3,
                        spaceBetween: 50
                    },
                    575: {
                        slidesPerView: 2,
                        spaceBetween: 20
                    }
                }
            });
        });
    }
}

city_slider.init();

//smooth scrolling
$(document).on('click', '.js-nav-scroll a', function (event) {
    event.preventDefault();

    var id = $(this).attr('href'),
        top = $(id).offset().top,
        headerHeight = $('.js-header').height();

    $('body,html').animate({scrollTop: top - headerHeight}, 800);
});

//menu fixed
$(document).on('scroll', function () {
    var headerHeight = 0;
    if ($(document).scrollTop() > headerHeight) {
        $('.js-global-wrapper').addClass('wrapper-padding');
        $('.js-header').addClass('header-fixed-top');
    }
    else {
        $('.js-global-wrapper').removeClass('wrapper-padding');
        $('.js-header').removeClass('header-fixed-top');
    }
});

//btn fancybox
$('.js-link-video').fancybox({
    buttons : [
        'close'
    ]
});

$('.js-projects').fancybox({
    buttons : [
        'slideShow',
        'fullScreen',
        'thumbs',
        //'share',
        'download',
        'zoom',
        'close'
    ]
});

//title modal
$(document).on('click', '.js-add-title-modal', function () {
    var name_section = $(this).attr('data-title-modal'),
        popup = $('#signupModal');
    popup.find('.js-title-modal').html(name_section);
});

$(document).on('click', '.js-btn-navbar', function () {
    $(this).toggleClass('show');
    $('.js-navbar-mobile').toggleClass('show');
    $('.js-global-wrapper').toggleClass('show');
    $('.js-header').toggleClass('show');
});