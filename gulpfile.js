// plugins for development
var gulp = require('gulp'),
	rimraf = require('rimraf'),
	jade = require('gulp-jade'), // compile jade
	sass = require('gulp-sass'),
	inlineimage = require('gulp-inline-image'),
	prefix = require('gulp-autoprefixer'),
	plumber = require('gulp-plumber'),
	dirSync = require('gulp-directory-sync'),
	browserSync = require('browser-sync').create(),
	concat = require('gulp-concat'),
	cssfont64 = require('gulp-cssfont64'),
	sourcemaps = require('gulp-sourcemaps'),
	postcss = require('gulp-postcss'),
	rigger = require('gulp-rigger'),
	assets  = require('postcss-assets');

// plugins for build
var purify = require('gulp-purifycss'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	csso = require('gulp-csso');

//plugins for testing
var html5Lint = require('gulp-html5-lint');
var reporter = require('postcss-reporter');
var stylelint = require('stylelint');
var postcss_scss = require("postcss-scss");

var assetsDir = 'assets/';
var outputDir = 'app/';
var buildDir = 'build/';

//----------------------------------------------------Compiling
// Компиляция PUG ->HTML
	gulp.task('jade', function () {
		gulp.src([assetsDir + 'jade/**/*.jade', '!' + assetsDir + 'jade/_*.jade', '!' + assetsDir + 'jade/tpl/_*.jade', '!' + assetsDir + 'jade/layout/_*.jade'])
			.pipe(plumber())
			.pipe(jade({pretty: true}))
			.pipe(gulp.dest(outputDir))
			.pipe(browserSync.stream());
	});

// Компиляция SASS -> CSS
	gulp.task('sass', function () {
		gulp.src([assetsDir + 'sass/**/*.sass', '!' + assetsDir + 'sass/**/_*.sass', assetsDir + 'sass/**/*.scss', '!' + assetsDir + 'sass/**/_*.scss'])
			.pipe(plumber())
			.pipe(sourcemaps.init())
			.pipe(sass())
			.pipe(inlineimage())
			.pipe(prefix('last 3 versions'))
			.pipe(postcss([assets({
				basePath:outputDir,
				loadPaths: ['i/']
			})]))
			.pipe(sourcemaps.write())
			.pipe(gulp.dest(outputDir + 'css/'))
			.pipe(browserSync.stream());
	});

// Сборка всех библеотек js
	gulp.task('jsConcat', function () {
		return gulp.src(assetsDir + 'js/all/**/*.js')
			.pipe(concat('all.js', {newLine: ';'}))
			.pipe(gulp.dest(outputDir + 'js/'))
			.pipe(browserSync.stream());
	});


// модульная сборка основного файла main.js
	gulp.task('jsMainModules', function () {
		return gulp.src([assetsDir + 'js/**/*.js', '!' + assetsDir + 'js/all/**/*.js'])
			.pipe(rigger())
			.pipe(gulp.dest(outputDir + 'js/'))
			.pipe(browserSync.stream());
	});


// Конвертация шрифтов
	gulp.task('fontsConvert', function () {
		return gulp.src([assetsDir + 'fonts/*.woff', assetsDir + 'fonts/*.woff2'])
			.pipe(cssfont64())
			.pipe(gulp.dest(outputDir + 'css/'))
			.pipe(browserSync.stream());
	});

//----------------------------------------------------Compiling###

//-------------------------------------------------Synchronization

// Копирование img из assets в app
	gulp.task('imageSync', function () {
		return gulp.src('')
			.pipe(plumber())
			.pipe(dirSync(assetsDir + 'img/', outputDir + 'img/', {printSummary: true}))
			.pipe(browserSync.stream());
	});

// Копирование fonts из assets в app
	gulp.task('fontsSync', function () {
		return gulp.src('')
			.pipe(plumber())
			.pipe(dirSync(assetsDir + 'fonts/', outputDir + 'fonts/', {printSummary: true}))
			.pipe(browserSync.stream());
	});

// Копирование js из assets в app
	gulp.task('jsSync', function () {
		return gulp.src(assetsDir + 'js/*.js')
			.pipe(plumber())
			.pipe(gulp.dest(outputDir + 'js/'))
			.pipe(browserSync.stream());
	});
//-------------------------------------------------Synchronization###


//watching files and run tasks
gulp.task('watch', function () {
	gulp.watch(assetsDir + 'jade/**/*.jade', ['jade']);
	gulp.watch(assetsDir + 'sass/**/*.scss', ['sass']);
	gulp.watch(assetsDir + 'sass/**/*.sass', ['sass']);
	gulp.watch(assetsDir + 'js/**/*.js', ['jsMainModules', 'jsSync']); 
	gulp.watch(assetsDir + 'js/all/**/*.js', ['jsConcat']);
	// gulp.watch(assetsDir + 'js/project modules/**/*.js', ['jsConcatModules']);
	gulp.watch(assetsDir + 'img/**/*', ['imageSync']);
	gulp.watch(assetsDir + 'fonts/**/*', ['fontsSync']);
});

//livereload and open project in browser
gulp.task('browser-sync', function () {
	browserSync.init({
		port: 1337,
		server: {
			baseDir: outputDir
		}
	});
});


//---------------------------------building final project folder
//clean build folder
gulp.task('cleanBuildDir', function (cb) {
	rimraf(buildDir, cb);
});


//minify images
gulp.task('imgBuild', function () {
	return gulp.src([outputDir + 'img/**/*', '!' + outputDir + 'img/sprite/*.svg'])
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest(buildDir + 'img/'))
});

// copy SVG
gulp.task('copySVG', function () {
	return gulp.src(outputDir + 'img/sprite/sprite.svg')
		.pipe(gulp.dest(buildDir + 'img/sprite/'))
});


//copy, minify css
gulp.task('cssBuild', function () {
	return gulp.src(outputDir + 'css/**/*')
		.pipe(purify([outputDir + 'js/**/*', outputDir + '**/*.html']))
		.pipe(csso())
		.pipe(gulp.dest(buildDir + 'css/'))
});


//copy fonts
gulp.task('fontsBuild', function () {
	return gulp.src(outputDir + '/fonts/**/*')
		.pipe(gulp.dest(buildDir + '/fonts/'))
});

//copy html
gulp.task('htmlBuild', function () {
	return gulp.src(outputDir + '**/*.html')
		.pipe(gulp.dest(buildDir))
});

//copy and minify js
gulp.task('jsBuild', function () {
	return gulp.src(outputDir + 'js/**/*')
		.pipe(uglify())
		.pipe(gulp.dest(buildDir + 'js/'))
});


//// --------------------------------------------If you need iconfont
// var iconfont = require('gulp-iconfont'),
// 	iconfontCss = require('gulp-iconfont-css'),
// 	fontName = 'iconfont';
// gulp.task('iconfont', function () {
// 	gulp.src([assetsDir + 'i/icons/*.svg'])
// 		.pipe(iconfontCss({
// 			path: 'assets/sass/templates/_icons_template.scss',
// 			fontName: fontName,
// 			targetPath: '../../sass/_icons.scss',
// 			fontPath: '../fonts/icons/',
// 			svg: true
// 		}))
// 		.pipe(iconfont({
// 			fontName: fontName,
// 			svg: true,
// 			formats: ['svg','eot','woff','ttf']
// 		}))
// 		.pipe(gulp.dest('assets/fonts/icons'));
// });

//--------------------------------------------If you need svg sprite
var svgSprite = require('gulp-svg-sprite'),
	svgmin = require('gulp-svgmin'),
	cheerio = require('gulp-cheerio'),
	replace = require('gulp-replace');

gulp.task('svgSpriteBuild', function () {
	return gulp.src(assetsDir + 'img/icons/*.svg')
	// minify svg
		.pipe(svgmin({
			js2svg: {
				pretty: true
			}
		}))
		// remove all fill and style declarations in out shapes
		.pipe(cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('[stroke]').removeAttr('stroke');
				$('[style]').removeAttr('style');
			},
			parserOptions: {xmlMode: true}
		}))
		// cheerio plugin create unnecessary string '&gt;', so replace it.
		.pipe(replace('&gt;', '>'))
		// build svg sprite
		.pipe(svgSprite({
			mode: {
				symbol: {
					sprite: "../sprite.svg",
					render: {
						scss: {
							dest:'../../../sass/base/_sprite.scss',
							template: assetsDir + "sass/extra/_sprite_template.scss"
						}
					},
					example: true
				}
			}
		}))
		.pipe(gulp.dest(assetsDir + 'img/sprite/'));
});

//testing your build files
gulp.task('validation', function () {
	return gulp.src(buildDir + '**/*.html')
		.pipe(html5Lint());
});

gulp.task('cssLint', function () {
	return gulp.src([assetsDir + 'sass/**/*.scss', '!' + assetsDir + 'sass/templates/*.scss'])
		.pipe(postcss(
			[
				stylelint(),
				reporter({ clearMessages: true })
			],
			{
				syntax: postcss_scss
			}
		));
});


gulp.task('default', ['jade', 'sass', 'imageSync', 'fontsSync', 'fontsConvert', 'jsConcat', 'jsMainModules', 'jsSync', 'watch', 'browser-sync']);

gulp.task('build', ['cleanBuildDir'], function () {
	gulp.start('copySVG', 'imgBuild',  'htmlBuild', 'jsBuild', 'cssBuild', 'fontsBuild');
});


